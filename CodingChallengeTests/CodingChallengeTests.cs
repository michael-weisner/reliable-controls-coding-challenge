﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RC.CodingChallenge.Classes;
using RC.CodingChallenge;
using System.Web;
using System.Collections.Generic;

namespace CodingChallengeTests
{
    [TestClass]
    public class CodingChallengeTests
    {
        /// <summary>
        /// Compare two linked lists by traversing the linkedlistnodes and making sure they're equal
        /// </summary>
        /// <param name="headA">The first node of the first list</param>
        /// <param name="headA">The first node of the second list</param>
        private bool CompareLists(LinkedListNode<DeviceEntry> headA, LinkedListNode<DeviceEntry> headB)
        {
            while (headA != null && headB != null && headA.Value.Equals(headB.Value))
            {
                headA = headA.Next;
                headB = headB.Next;
            }

            return headA == headB;
        }

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Make sure that ParseEvents method creates a new device and stores the data correctly
        /// This will fail if the Parse Events is using a thread because this will return before the thread is done
        /// </summary>
        [TestMethod]
        public void ParseEvents_WithData_UpdatesDevice()
        {
            // create data
            Device device = new Device("TestingDevice");
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 3, 0, 0), DEVICE_STATUS.Three));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 4, 0, 0), DEVICE_STATUS.Two));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 5, 0, 0), DEVICE_STATUS.Three));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 6, 0, 0), DEVICE_STATUS.Zero));

            StreamReader reader = new StreamReader(GenerateStreamFromString("2017-09-16 03:00:00\t3\n2017-09-16 04:00:00\t2\n2017-09-16 05:00:00\t3\n2017-09-16 06:00:00\t0\n"));
            DeviceHandler deviceHandler = new DeviceHandler();

            //make sure the device doesn't already exist
            Global.deviceList.Remove(device.ID);

            // run function
            deviceHandler.ParseEvents(device.ID, reader);

            //check results
            Assert.IsTrue(Global.deviceList.ContainsKey(device.ID), "Device not added to global device list");
            Assert.AreEqual(device.Entries.Count, Global.deviceList[device.ID].Entries.Count, 0, "Not enough entries were added");
            Assert.IsTrue(CompareLists(device.Entries.First, Global.deviceList[device.ID].Entries.First), "Entries do not match");

            //clean up
            Global.deviceList.Remove(device.ID);
        }

        /// <summary>
        /// Make sure that ParseEvents method throws an error if device id is null
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void ParseEvent_WithNullDeviceID()
        {
            // create data
            StreamReader reader = new StreamReader(GenerateStreamFromString("2017-09-16 03:00:00\t3\n2017-09-16 04:00:00\t2\n2017-09-16 05:00:00\t3\n2017-09-16 06:00:00\t0\n"));
            DeviceHandler deviceHandler = new DeviceHandler();

            // run function
            deviceHandler.ParseEvents(null, reader);

            // assert is handled by ExpectedException
        }

        /// <summary>
        /// Make sure that ParseEvents method throws an error if the eventLog is null
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void ParseEvent_WithNullStreamReader()
        {
            // create data
            DeviceHandler deviceHandler = new DeviceHandler();

            // run function
            deviceHandler.ParseEvents("TestingDevice", null);

            // assert is handled by ExpectedException
        }

        /// <summary>
        /// Make sure that RefreshWebSockets throws an error if deviceID is null
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void RefreshWebSockets_WithNullDeviceID()
        {
            // create data
            DeviceHandler deviceHandler = new DeviceHandler();

            // run function
            deviceHandler.RefreshWebSockets(null);

            // assert is handled by ExpectedException
        }

        /// <summary>
        /// Make sure that GetEventCount throws an error if deviceID is null
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void GetEventCount_WithNullDeviceID()
        {
            // create data
            DeviceHandler deviceHandler = new DeviceHandler();

            // run function
            deviceHandler.GetEventCount(null);

            // assert is handled by ExpectedException
        }

        /// <summary>
        /// Make sure that GetEventCount returns a 0 if there are no errors in the eventLog
        /// </summary>
        [TestMethod]
        public void GetEventCount_WithDataNoFault_ReturnsZero()
        {
            // create data
            int expected = 0;
            Device device = new Device("TestingDeviceZero");
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 3, 0, 0), DEVICE_STATUS.Three));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 4, 0, 0), DEVICE_STATUS.Two));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 5, 0, 0), DEVICE_STATUS.Three));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 6, 0, 0), DEVICE_STATUS.One));

            //make sure the device doesn't already exist before adding it manually
            Global.deviceList.Remove(device.ID);
            Global.deviceList.Add(device.ID, device);

            DeviceHandler deviceHandler = new DeviceHandler();

            // run function
            int result = deviceHandler.GetEventCount(device.ID);

            //check results
            Assert.AreEqual(expected, result, 0, "Not the correct number of faults detected");

            //clean up
            Global.deviceList.Remove(device.ID);
        }

        /// <summary>
        /// Make sure that GetEventCount returns a 1 if there is one error in the eventLog
        /// </summary>
        [TestMethod]
        public void GetEventCount_WithDataOneFault_ReturnsOne()
        {
            // create data
            int expected = 1;
            Device device = new Device("TestingDeviceOne");
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 3, 0, 0), DEVICE_STATUS.Three));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 4, 0, 0), DEVICE_STATUS.Two));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 5, 0, 0), DEVICE_STATUS.Three));
            device.Entries.AddLast(new DeviceEntry(new DateTime(2017, 09, 16, 6, 0, 0), DEVICE_STATUS.Zero));

            //make sure the device doesn't already exist before adding it manually
            Global.deviceList.Remove(device.ID);
            Global.deviceList.Add(device.ID, device);

            DeviceHandler deviceHandler = new DeviceHandler();

            // run function
            int result = deviceHandler.GetEventCount(device.ID);

            //check results
            Assert.AreEqual(expected, result, 0, "Not the correct number of faults detected");

            //clean up
            Global.deviceList.Remove(device.ID);
        }
    }
}
