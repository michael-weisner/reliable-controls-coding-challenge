﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="RC.CodingChallenge.index" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="contentHead">
    <script type="text/javascript">
        $(function () {
            Data.Connect();
        });

        window.onbeforeunload = function () {
            Data.Disconnect();
        };

        function SocketOpened() {
            //when the socket is opened send a request for all data
            Data.SendMessage([{ command: 'all', args: {} }]);
        }

        function ReceiveMessage(message) {
            switch (message.command) {
                case 'update':
                    {
                        //the server has sent us an update for a device
                        var id = message.args.id;
                        var faults = message.args.faults;

                        //check and see if this row exists
                        var today = new Date();
                        var $tr = $('#tblDevices tr[device-id="' + id + '"]');
                        if ($tr.length == 0) {
                            //create the row
                            $tr = $('<tr>')
                                .attr('device-id', id)
                                .append($('<td>').html('<button class="btn" type="button" onclick="refreshDevice(\'' + id + '\');">Refresh</button>'))
                                .append($('<td>').html(id))
                                .append($('<td>').addClass('fault-count').html(faults))
                                .append($('<td>').addClass('last-report').html(today))
                                .appendTo($('#tblDevices tbody'));
                        } else {
                            //alter the data for the row
                            $('.fault-count', $tr).html(faults);
                            $('.last-report', $tr).html(today);
                        }

                    } break;
            }
        }

        function refreshDevice(deviceID) {
            //ask the server to refresh the dat on this device
            Data.SendMessage([{ command: 'refresh', args: { deviceID: deviceID }}]);
        }
    </script>

    <style type="text/css">
        #tblDevices{
            width:900px;
            margin:auto;
            margin-top:20px;
            background-color: #FFFFFF;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="contentMenu" runat="server">
    <button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <span class="brand" style="margin-left:-20px;">Reliable Controls Coding Challenge</span>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="contentMain">
    <table id="tblDevices" class="table table-condensed table-solidheader table-bordered">
        <thead>
            <tr>
                <th></th>
                <th>Device ID</th>
                <th>Fault Count</th>
                <th>Last Report</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</asp:Content>
