﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RC.CodingChallenge.com;

namespace RC.CodingChallenge
{
    public partial class dummy_device : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Upload_File(object sender, EventArgs e)
        {
            using (ChallengeService service = new ChallengeService())
            {
                service.ReceiveDataFromFile(txtDeviceID.Text, fileUploadLog.FileBytes);
            }
        }
    }
}