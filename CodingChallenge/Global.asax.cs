﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Fleck;
using RC.CodingChallenge.Classes;

namespace RC.CodingChallenge
{
    public class Global : System.Web.HttpApplication
    {
        public static Dictionary<string, Device> deviceList = new Dictionary<string,Device>();
        public static DeviceHandler deviceHandler = new DeviceHandler();

        //Fleck websocket server, I'm using this because the server I develop on doesn't support websockets.
        //The html code should remain the same, and an actual websocket service isn't that much different.
        private static WebSocketServer websocketServer;

        protected void Application_Start(object sender, EventArgs e)
        {
            //Start the webserver socket, basically all traffic will get routed through the RCSocketManager Class
            websocketServer = new Fleck.WebSocketServer("ws://127.0.0.1:443");
            websocketServer.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    Console.WriteLine("Socket Open!");
                    RCWebSocketManager.AddSocket(socket);
                };
                socket.OnClose = () =>
                {
                    Console.WriteLine("Socket Close!");
                    RCWebSocketManager.RemoveSocket(socket);
                };
                socket.OnMessage = (message) =>
                {
                    RCWebSocketManager.Receive(socket, message);
                };
            });
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            websocketServer.Dispose();
        }
    }
}