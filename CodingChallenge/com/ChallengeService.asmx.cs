﻿///<summary>
///Challenge Service is a webservice to accept device data in multiple different ways and send it to the LogHandler
///</summary>
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using RC.CodingChallenge.Classes;

namespace RC.CodingChallenge.com
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class ChallengeService : System.Web.Services.WebService
    {
        /*
        private string GenerateTestData(int rows)
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            DateTime entryDate = DateTime.Now;
            for (int i = 0; i < rows; i++)
            {
                int state = random.Next(0, 4);
                if ((state == 0 || state == 1) && random.NextDouble() > .25)
                    state = random.Next(0, 4);
                string entry = entryDate.ToString("yyyy-MM-dd H:mm:ss") + "\t" + state + "\n";
                sb.Append(entry);
                entryDate = entryDate.AddSeconds(random.Next(1, 18000));
            }

            return sb.ToString();
        }
         * */

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ReceiveDataFromString(string deviceID, string Data)
        {
            ReceiveDataFromStream(deviceID, new StreamReader(GenerateStreamFromString(Data)));
        }

        [WebMethod]
        public void ReceiveDataFromFile(string deviceID, byte[] FileBytes)
        {
            ReceiveDataFromStream(deviceID, new StreamReader(new MemoryStream(FileBytes)));
        }

        [WebMethod]
        public void ReceiveDataFromStream(string deviceID, StreamReader eventLog)
        {
            Global.deviceHandler.ParseEvents(deviceID, eventLog);
        }
    }
}
