﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="dummy-device.aspx.cs" Inherits="RC.CodingChallenge.dummy_device" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentHead" runat="server">
    <script type="text/javascript" src="js/date.format.js"></script>
    <script type="text/javascript">
        var startDate = new Date();

        $(function () {
            Data.Connect();

        });

        window.onbeforeunload = function () {
            Data.Disconnect();
        };

        function getRandom(min, max) {
            return parseInt((Math.random() * (max - min)) + min);
        }

        function generateData() {
            var result = '';
            for (var i = 0; i < 200; i++) {
                var state = getRandom(0, 4);
                if ((state == 0 || state == 1) && Math.random() > .25)
                    state = getRandom(0, 4);
                result += dateFormat(startDate, 'yyyy-mm-dd HH:MM:ss') + '\t' + state + '\n';   //entryDate.ToString("yyyy-MM-dd H:mm:ss") + "\t" + state + "\n";
                startDate.setSeconds(startDate.getSeconds() + getRandom(1, 18000));
            }
            $('#txtData').html(result);
        }

        //This shows that the WebService can be accessed via AJAX
        function sendData() {
            $.ajax({
                type: 'POST',
                url: 'com/ChallengeService.asmx/ReceiveDataFromString',
                data: (JSON.stringify({ deviceID: $('#<%= txtDeviceID.ClientID %>').val(), Data: $('#txtData').val() })),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    $.log('send data success[' + sendID + ']');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.log('error: ' + errorThrown);
                }
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentMenu" runat="server">
    <button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <span class="brand" style="margin-left:-20px;">Add Dummy Data to Device</span>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="contentMain" runat="server">
    <div style="padding:10px;">
        DeviceID <asp:TextBox runat="server" ID="txtDeviceID" CssClass="span4"></asp:TextBox><br />
        <div class="alert">Add from file:</div>
        <asp:FileUpload ID="fileUploadLog" runat="server"/>
        <asp:Button runat="server" ID="btnUploadFile" UseSubmitBehavior="true" OnClick="Upload_File" Text="Upload File" /><br />
        <div class="alert">Add from text:</div>
        <textarea id="txtData" cols="50" rows="20"></textarea><br />
        <button class="btn" type="button" onclick="generateData();">Generate Data</button>
        <button class="btn" type="button" onclick="sendData();">Send Data</button>
    </div>
</asp:Content>
