﻿///<summary>
///This class represents the Device and is meant to contain the running colection of events associate with the device
///</summary>
///<remarks>
///The class holds _lastCount which should be a record of the number of errors last counted.
///This is based off the GetEventCount** methods and may not accurately reflect the actual number of faults but is used primarily when a website request information, saving the data is less of a resource drain
///
/// The data from the event log is stored in a linked list to maintain the date structure of the event.
///</remarks>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace RC.CodingChallenge.Classes
{
    public class Device
    {
        public enum TRIGGERED_STATE
        {
            none,
            firstState,
            secondState
        }

        string _id;
        //TODO: extend the LinkedList to refresh the _lastCount whenthe Add method is invoked
        //TODO: extend the LinkedList to maintain the correct sequence of dates, just in case we get a set of data that is out of sync
        LinkedList<DeviceEntry> _entries = new LinkedList<DeviceEntry>();
        int _lastCount;

        public Device(string deviceID)
        {
            _id = deviceID;
        }

        public string ID
        {
            get { return _id; }
        }

        public LinkedList<DeviceEntry>  Entries
        {
            get { return _entries; }
        }

        public int LastCount
        {
            get { return _lastCount; }
        }

        /// <summary>
        /// This converts the list of device entries into a string that can be used with a regex pattern
        /// </summary>
        public string GetEventSequence()
        {
            LinkedListNode<DeviceEntry> node = _entries.First;
            if (node == null)
                return "";
            LinkedListNode<DeviceEntry> next = node.Next;

            StringBuilder sequence = new StringBuilder(((int)node.Value.Status).ToString());

            while (next != null)
            {
                if(next.Value.Status != node.Value.Status){
                    //this is an update
                    if (node.Value.Status == DEVICE_STATUS.Three)
                    {
                        //if it's staus 3 then get the duration and record it if it's more than 5 minutes
                        double duration = (next.Value.EntryDate - node.Value.EntryDate).TotalMinutes;
                        if (duration >= 5)
                            sequence.Append("T");
                    }
                    //check out the next node
                    node = next;
                    sequence.Append((int)node.Value.Status);
                }
                next = next.Next;   //That was an unintentionally confusing statement.
            }

            return sequence.ToString();
        }

        /// <summary>
        /// This uses a string based on the list of device entries and applies a regex string to it to find any matches
        /// While possibly slower, and inefficient due to multiple passes of the data, it does give the flexibility of applying a custom pattern
        /// </summary>
        /// <param name="pattern">This is the regex pattern that will be used to match against</param>
        public int GetEventCountRegEx(string pattern)
        {
            string eventSequence = GetEventSequence();
            _lastCount = Regex.Matches(eventSequence, pattern).Count;
            return _lastCount;
        }

        /// <summary>
        /// This iterates through the list of entries and tries to find a match to the fault sequnce.
        /// Sequence is as follows:
        /// Status 3 for 5 minutes or more
        /// Followed immediately by status 2
        /// Followed by any number of Status 2 or Status 3
        /// Followed immediately by Status 0
        /// A fault is recorded if that sequence is observed
        /// The triggered state is used to determine at what point in the fault sequence we're currently in.
        /// </summary>
        public int GetEventCountIterative()
        {
            int faultCount = 0;
            TRIGGERED_STATE triggeredState = TRIGGERED_STATE.none;

            LinkedListNode<DeviceEntry> node = _entries.First;
            LinkedListNode<DeviceEntry> next = node.Next;

            while (next != null)
            {
                //check to see if the status has changed to ignore any heartbeats
                if (next.Value.Status != node.Value.Status)
                {
                    if (node.Value.Status == DEVICE_STATUS.Three && triggeredState == TRIGGERED_STATE.none)
                    {
                        //If we're not suspicious of a fault yet, and the device just switched to state 3 then get suspicious if it lasts longer than 5 minutes 
                        double duration = (next.Value.EntryDate - node.Value.EntryDate).TotalMinutes;
                        if (duration >= 5)
                            triggeredState = TRIGGERED_STATE.firstState;
                    }
                    else if (node.Value.Status == DEVICE_STATUS.Two && triggeredState == TRIGGERED_STATE.firstState)
                    {
                        //The second part of the fault sequence
                        triggeredState = TRIGGERED_STATE.secondState;
                    }
                    else if ((node.Value.Status == DEVICE_STATUS.Zero || node.Value.Status == DEVICE_STATUS.Two || node.Value.Status == DEVICE_STATUS.Three)
                        && triggeredState == TRIGGERED_STATE.secondState)
                    {
                        //any of 0, 2, 3 as a status qualifies at this point for the next part of the sequence
                        if (node.Value.Status == DEVICE_STATUS.Zero)
                        {
                            //but only record a true fault if the status becomes zero, and reset the triggered state
                            faultCount++;
                            triggeredState = TRIGGERED_STATE.none;
                        }
                    }
                    else
                    {
                        //false alarm, whatever status change happened won't lead to a fault
                        triggeredState = TRIGGERED_STATE.none;
                    }

                    //we've passed any heartbeats so update the node we're focussing on
                    node = next;
                }
                next = next.Next;
            }

            //check the last node to see if it completes a fault with a 0 status
            if(node.Value.Status == DEVICE_STATUS.Zero && triggeredState == TRIGGERED_STATE.secondState){
                faultCount++;
            }

            _lastCount = faultCount;
            return faultCount;
        }
    }
}