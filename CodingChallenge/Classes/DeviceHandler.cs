﻿///<summary>
///This class manages the data from the data stream.
///Implements the EventCounter Interface
///And broadcasts a data change to any websockets attached to the website
///</summary>
///<remarks>
///Threading has been added to handle the parsing of the stream in case of heavy traffic.
///</remarks>
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace RC.CodingChallenge.Classes
{
    public class ThreadData
    {
        public string deviceID;
        public StreamReader eventLog;

        public ThreadData() {}
        public ThreadData(string deviceID, StreamReader eventLog)
        {
            this.deviceID = deviceID;
            this.eventLog = eventLog;
        }
    }
    public class DeviceHandler : IEventCounter 
    {
        //The current fault pattern (starts with a 3 longer that 5 minutes, followed by a 2, followed by any number of 2, 3, or T because we might have more 3's greater than 5 minutes, followe by a 0)
        private string faultPattern = "3T2(2|3|T)*0";
        public DeviceHandler(){}
        public DeviceHandler(string deviceID, StreamReader eventLog)
        {
            ParseEvents(deviceID, eventLog);
            
        }

        /// <summary>
        /// Implements the EventCounter Interface method
        /// </summary>
        /// <param name="deviceID">ID of the device that the log is associated with (ex: "HV1")</param>
        /// <param name="eventLog">A stream of lines representing time/value recordings.</param>
        public void ParseEvents(string deviceID, StreamReader eventLog)
        {
            if (deviceID == null || deviceID == "")
                throw new System.NullReferenceException("Device ID is null");

            if (eventLog == null)
                throw new System.NullReferenceException("EventLog StreamReader is null");

            //non threading version
            doParseEvent(deviceID, eventLog);

            /*
            //Because we're using a thread, we should make a copy of the stream just in case the stream is terminated at the source before the thread is finished
            ThreadData data = new ThreadData() { deviceID = deviceID, eventLog = eventLog };
            Thread thread = new Thread(new ParameterizedThreadStart(threadParseEvent));
            //CopyStream(eventLog.BaseStream, data.eventLog);
            thread.Start(data);
             * */
        }

        /// <summary>
        /// Copies the contents of one stream to another
        /// </summary>
        /// <param name="input">The stream to pull the data from</param>
        /// <param name="output">The stream to add data to</param>
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[16*1024];
            int read;
            while((read = input.Read (buffer, 0, buffer.Length)) > 0)
            {
                output.Write (buffer, 0, read);
            }
        }

        /// <summary>
        /// The thread enabled method that does the work of parsing the data stream
        /// </summary>
        /// <param name="data">A ThreadData class that contains both the deviceID and eventLog data</param>
        private void threadParseEvent(object data)
        {
            ThreadData td = (ThreadData)data;
            doParseEvent(td.deviceID, td.eventLog);
        }

        /// <summary>
        /// Parse and accumulate event information from the given log data.
        /// This is separate so that both the thread method and interface can use the same code
        /// </summary>
        /// <param name="deviceID">ID of the device that the log is associated with (ex: "HV1")</param>
        /// <param name="eventLog">A stream of lines representing time/value recordings.</param>
        private void doParseEvent(string deviceID, StreamReader eventLog)
        {
            //Check the global list to see if we're already storing data for this device
            Device device;
            if (!Global.deviceList.ContainsKey(deviceID))
            {
                //if we aren't then create a new one
                device = new Device(deviceID);
                Global.deviceList.Add(deviceID, device);
            }
            else
                device = Global.deviceList[deviceID];

            //create a streamreader form the copied stream
            StreamReader sr = eventLog; // new StreamReader(td.eventLog);

            //read each line in the event log and use it to create a new device entry, adding that to the device in question
            string buffer = sr.ReadLine();
            while (buffer != null)
            {
                try
                {
                    device.Entries.AddLast(new DeviceEntry(buffer));
                }
                catch (Exception err)
                {

                }
                buffer = sr.ReadLine();
            }
            RefreshWebSockets(deviceID);
        }

        /// <summary>
        /// Contacts the WebSocketManager and broadcasts an update message to any pages that are listening
        /// </summary>
        /// <param name="deviceID">ID of the device that the log is associated with (ex: "HV1")</param>
        public void RefreshWebSockets(string deviceID)
        {
            if (deviceID == null || deviceID == "")
                throw new System.NullReferenceException("Device ID is null");

            //if I have any websockets open then GetEventCount and send the data to the sockets
            RCWebSocketManager.Broadcast(new List<RCWebSocketManager.Message>() {
                new RCWebSocketManager.Message(){ 
                    command = "update", 
                    args = new Dictionary<string, object>() { { "id", deviceID }, {"faults", GetEventCount(deviceID) } } 
                }
            });
        }

        /// <summary>
        /// Implements the EventCounter Interface method
        /// Gets the current count of events detected for the given device
        /// Currently set to using the RegEx version of the 
        /// </summary>
        /// <returns>An integer representing the number of detected events</returns>
        public int GetEventCount(string deviceID)
        {
            //check to make sure the deviceID exists
            //TODO: discuss if throwing an error is the correct approach.  Perhaps we should exit gracefully instead with a -1
            if (deviceID == null || deviceID == "")
                throw new System.NullReferenceException("Device ID is null");

            if(!Global.deviceList.ContainsKey(deviceID))
                throw new Exception("Device does not exist");

            return Global.deviceList[deviceID].GetEventCountRegEx(faultPattern);
        }
    }
}