﻿///<summary>
///This handles all the websocket traffic both inbound and outbound data
///</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Fleck;

namespace RC.CodingChallenge.Classes
{
    public class RCWebSocketManager
    {
        public static Dictionary<Guid, RCWebSocket> allClients = new Dictionary<Guid, RCWebSocket>();   //This is a record of all current connections

        public class RCWebSocket
        {
            public Guid ID;
            public IWebSocketConnection socket;
        }

        public class Message
        {
            public string command;
            public Dictionary<string, object> args;
        }

        /// <summary>
        /// Stores the socket, and sends a message to log a message that it has connected witht he server
        /// </summary>
        /// <param name="Socket">The socket connection that's made contact with the server</param>
        public static void AddSocket(IWebSocketConnection Socket)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            allClients.Add(Socket.ConnectionInfo.Id, new RCWebSocket() { ID = Socket.ConnectionInfo.Id, socket = Socket });
            Socket.Send(jsonSerializer.Serialize(new List<Message>() { 
                new Message(){
                    command = "log", 
                    args = new Dictionary<string, object>() { { "msg", Socket.ConnectionInfo.Id + " has connected." } }
                } 
            }));
        }

        /// <summary>
        /// Removes the Socket from the list of connections when it disconnects form the server
        /// </summary>
        /// <param name="Socket">The socket connection that's disconnected</param>
        public static void RemoveSocket(IWebSocketConnection Socket)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            allClients.Remove(Socket.ConnectionInfo.Id);
            Socket.Send(jsonSerializer.Serialize(new List<Message>() {
                new Message() { 
                    command = "log", 
                    args = new Dictionary<string, object>() { { "msg", Socket.ConnectionInfo.Id + " has disconnected." } } 
                }
            }));
        }

        /// <summary>
        /// Handles receiving data from a socket
        /// </summary>
        /// <param name="Socket">The socket connection that's communicating with the server</param>
        /// <param name="Data">The data that the socket is trying to send in the form of a List of Messages</param>
        public static void Receive(IWebSocketConnection Socket, string Data)
        {
            //[{'command':(refresh|all|update|log), 'args':{name:value}}]
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            try
            {
                //attempt to deserialise the message
                List<Message> msgList = jsonSerializer.Deserialize<List<Message>>(Data);

                foreach (Message msg in msgList)
                {
                    switch (msg.command)
                    {
                        case "refresh":
                            {
                                //the socket is asking for the data for this device to be refreshed expect args to equal {'deviceID':id}
                                if (msg.args.ContainsKey("deviceID"))
                                {
                                    string deviceID = msg.args["deviceID"].ToString();
                                    if (Global.deviceList.ContainsKey(deviceID))
                                    {
                                        //if there is a device listed under that ID then send an update message to the socket.
                                        Socket.Send(jsonSerializer.Serialize(new List<Message>() { 
                                            new Message(){ 
                                                command = "update", 
                                                args = new Dictionary<string, object>() { { "id", deviceID }, {"faults", Global.deviceList[deviceID].LastCount} } 
                                            }
                                        }));
                                    }
                                }
                            } break;
                        case "all":
                            {
                                //the socket is asking for a refresh of all the data, don't expect any args
                                List<Message> messages = new List<Message>();
                                foreach (KeyValuePair<string, Device> device in Global.deviceList)
                                {
                                    //loop through each device and add the faults to a list of messages
                                    messages.Add(
                                        new Message(){ 
                                                command = "update", 
                                                args = new Dictionary<string, object>() { { "id", device.Key }, {"faults", device.Value.LastCount} } 
                                            }
                                    );
                                }

                                Socket.Send(jsonSerializer.Serialize(messages));
                            }break;
                    }
                }
            }
            catch (Exception e)
            {
                //send a log error to the socket if it had troubles with the message
                string data = jsonSerializer.Serialize(new List<Message>() { 
                    new Message(){ 
                        command = "log", 
                        args = new Dictionary<string, object>() { { "msg", "Socket Error: " + e.Message } } 
                    }
                });
                Socket.Send(data);
            }
        }

        /// <summary>
        /// Sends a message to all clients currently connected
        /// </summary>
        /// <param name="Messages">A list of messages to send every connection</param>
        /// <param name="Ignore">An optional Guid to ignore so that one client can send to the rest without an echo</param>
        public static void Broadcast(List<Message> Messages, Guid? Ignore = null)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            if (Ignore == null) //Send to everyone
                allClients.ToList().ForEach(s => s.Value.socket.Send(jsonSerializer.Serialize(Messages)));
            else //Send to every client that doesn't have the ignore ID
                allClients.Where(s => s.Value.socket.ConnectionInfo.Id != (Guid)Ignore).ToList().ForEach(s => s.Value.socket.Send(jsonSerializer.Serialize(Messages)));
        }
    }
}