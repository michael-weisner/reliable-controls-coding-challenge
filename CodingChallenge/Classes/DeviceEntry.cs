﻿///<summary>
///This class represents the Device Entry data
///The constructor should parse the string fromt he log file and fill in the data
///</summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RC.CodingChallenge.Classes
{
    public enum DEVICE_STATUS
    {
        Zero = 0,
        One = 1,
        Two = 2,
        Three = 3
    }

    public class DeviceEntry
    {
        DateTime _entry;
        DEVICE_STATUS _status;

        public DeviceEntry(DateTime entryDate, DEVICE_STATUS status)
        {
            _entry = entryDate;
            _status = status;
        }

        public DeviceEntry(string logEntry)
        {
            //parse the entry line, split on the tab character
            string[] parts = logEntry.Split(new char[]{'\t'}, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                throw new Exception("Too many columns in log entry");

            if (!DateTime.TryParse(parts[0], out _entry))
                throw new Exception("Invalid date/time in log entry");

            int deviceStatus = int.Parse(parts[1]);
            if (deviceStatus < 0 || deviceStatus > 3)
                throw new IndexOutOfRangeException("Device Status is out of range");

            _status = (DEVICE_STATUS)deviceStatus;
        }

        public DateTime EntryDate
        {
            get { return _entry; }
            set { _entry = value; }
        }

        public DEVICE_STATUS Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override bool Equals(object obj)
        {
            DeviceEntry de = (DeviceEntry)obj;
            return de.EntryDate == this.EntryDate && de.Status == this.Status;
        }
    }
}