﻿Data = {
    wsUri: 'ws://localhost:443',
    socket: null,

    Connect: function () {
        Data.socket = new WebSocket(Data.wsUri);
        Data.socket.onopen = function (evt) { Data.onOpen(evt); };
        Data.socket.onclose = function (evt) { Data.onClose(evt); };
        Data.socket.onmessage = function (evt) { Data.onMessage(evt); };
        Data.socket.onerror = function (evt) { Data.onError(evt); };
    },

    Disconnect: function () {
        Data.socket.close();
    },

    onOpen: function (evt) {
        $.log('WebSocket Opened');
        //check and see if the webpage has registered a function to handle the Socket Opening
        if (SocketOpened) {
            SocketOpened(evt);
        }
    },

    onClose: function (evt) {
        $.log('WebSocket Closed');
    },

    onMessage: function (evt) {
        $.log('socket rcvd: ' + evt.data.toString());

        //parse the response form the server
        //if it's a log request then don't both sending it on
        var response = JSON.parse(evt.data);

        $(response).each(function () {
            if (this.command === 'log')
                $.log('Log Msg:' + this.args.msg);
            else {
                //see if the web page has registered a function to receive messages, and call it with the data
                if (ReceiveMessage) {
                    ReceiveMessage(this);
                }
            }
        });

    },

    onError: function (evt) {
        $.log('WebSocket Error:' + evt + " at " + evt.currentTarget.url);
    },

    SendMessage: function (data) {
        //check to see if the socket is open then send the data
        if (Data.socket.readyState !== WebSocket.OPEN)
            return;

        Data.socket.send(JSON.stringify(data));
    }
};